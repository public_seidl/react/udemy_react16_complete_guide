import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Assignment01 from './Assignment_1/Assignment01';
import registerServiceWorker from './registerServiceWorker';

//ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
